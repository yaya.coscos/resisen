/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isen.resisen;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.shape.Rectangle;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author ZFRV3109
 */
public class PrimaryController implements Initializable {

    @FXML
    public ComboBox<String> nbBande;
    @FXML
    public ComboBox<String> centaine;
    @FXML
    public ComboBox<String> dizaine;
    @FXML
    public ComboBox<String> unite;
    @FXML
    public ComboBox<String> multiplicateur;
    @FXML
    public ComboBox<String> tolerance;
    @FXML
    public ComboBox<String> temp;
    @FXML
    public Rectangle bCentaine;
    @FXML
    public Rectangle bTemp;
    @FXML
    public Rectangle bDizaine;
    @FXML
    public Rectangle bUnite;
    @FXML
    public Rectangle bMult;
    @FXML
    public Rectangle bTolerance;
    @FXML
    public Label lCentaine;
    @FXML
    public Label lDizaine;
    @FXML
    public Label lUnite;
    @FXML
    public Label lMulti;
    @FXML
    public Label lTolerance;
    @FXML
    public Label lTemp;
    @FXML
    public Label valeurTolerance;
    @FXML
    public Label titreResistance;
    @FXML
    public TextField valeurResistance;
    @FXML
    public Label uniteResistance;
    @FXML
    public Label titreTolerance;
    @FXML
    public Label titreTemp;
    @FXML
    public Label uniteTemp;
    @FXML
    public Label uniteTolerance;
    @FXML
    public Label valeurTemp; 
    
    public static double vTolerance;
    public static double vTemp;
    public static double vMult;
    public static double vCentaine;
    public static double vDizaine;
    public static double vUnite;
    public static double vResitance;
    public static int nombreBande;
    public String[] cUnite;
    public String[] cDizaine;
    public String[] cCentaine;
    public String[] cMult;
    public String[] cTolerance;
    public String[] cTemp;

        ObservableList<String> nbBandeList //
            = FXCollections.observableArrayList("3 bandes", "4 bandes", "5 bandes", "6 bandes");
        ObservableList<String> firstDigitList //
                = FXCollections.observableArrayList("1 Marron", "2 Rouge", "3 Orange", "4 Jaune","5 Vert","6 Bleu","7 Violet","8 Gris","9 Blanc");
        ObservableList<String> otherDigitList //
                = FXCollections.observableArrayList("0 Noir","1 Marron", "2 Rouge", "3 Orange", "4 Jaune","5 Vert","6 Bleu","7 Violet","8 Gris","9 Blanc");
        ObservableList<String> multiList //
                = FXCollections.observableArrayList("0.01 Argent", "0.1 Or", "1 Noir", "10 Marron", "100 Rouge","1000 Orange","10000 Jaune","100000 Vert","1000000 Bleu","10000000 Violet","100000000 Gris","1000000000 Blanc");
        ObservableList<String> toleranceList //
                = FXCollections.observableArrayList("10 Argent", "5 Or",  "1 Marron", "2 Rouge","3 Orange","4 Jaune","0.5 Vert","0.25 Bleu","0.1 Violet","0.05 Gris");
        ObservableList<String> tempList //
                = FXCollections.observableArrayList("200 Noir",  "100 Marron", "50 Rouge","15 Orange","25 Jaune","20 Vert","10 Bleu","5 Violet","1 Gris");

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO

        nbBande.setItems(nbBandeList);
        init();
        
    }
    
    /*public void setTroisBande(){
        
        dizaine.setItems(firstDigitList);
        unite.setItems(otherDigitList);
        multiplicateur.setItems(multiList);
        
        dizaine.setVisible(true);
        unite.setVisible(true);
        multiplicateur.setVisible(true);
        bDizaine.setVisible(true);
        bUnite.setVisible(true);
        bMult.setVisible(true);

        
        lDizaine.setVisible(true);
        lUnite.setVisible(true);
        lMulti.setVisible(true);
        
        lDizaine.setText("1st Digit");
        lUnite.setText("2nd Digit");
        
        
        titreResistance.setVisible(true);
        valeurResistance.setVisible(true);
        uniteResistance.setVisible(true);
        titreTolerance.setVisible(true);
        valeurTolerance.setVisible(true);
        uniteTolerance.setVisible(true);
        
        
        vTolerance = 20;
        valeurTolerance.setText(String.valueOf(vTolerance));
    }
    
    public void setQuatreBande(){
        dizaine.setItems(firstDigitList);
        unite.setItems(otherDigitList);
        multiplicateur.setItems(multiList);
        tolerance.setItems(toleranceList);
        
        dizaine.setVisible(true);
        unite.setVisible(true);
        multiplicateur.setVisible(true);
        tolerance.setVisible(true);
        bDizaine.setVisible(true);
        bUnite.setVisible(true);
        bMult.setVisible(true);
        bTolerance.setVisible(true);
        
        lDizaine.setVisible(true);
        lUnite.setVisible(true);
        lMulti.setVisible(true);
        lTolerance.setVisible(true);
        
        lDizaine.setText("1st Digit");
        lUnite.setText("2nd Digit");
        
        titreResistance.setVisible(true);
        valeurResistance.setVisible(true);
        uniteResistance.setVisible(true);
        titreTolerance.setVisible(true);
        valeurTolerance.setVisible(true);
        uniteTolerance.setVisible(true);

    }
    
    public void setCinqBande(){
        centaine.setItems(firstDigitList);
        dizaine.setItems(otherDigitList);
        unite.setItems(otherDigitList);
        multiplicateur.setItems(multiList);
        tolerance.setItems(toleranceList);
        
        
        centaine.setVisible(true);
        dizaine.setVisible(true);
        unite.setVisible(true);
        multiplicateur.setVisible(true);
        tolerance.setVisible(true);
        bCentaine.setVisible(true);
        bDizaine.setVisible(true);
        bUnite.setVisible(true);
        bMult.setVisible(true);
        bTolerance.setVisible(true);
        
        lCentaine.setVisible(true);
        lDizaine.setVisible(true);
        lUnite.setVisible(true);
        lMulti.setVisible(true);
        lTolerance.setVisible(true);
        
        titreResistance.setVisible(true);
        valeurResistance.setVisible(true);
        uniteResistance.setVisible(true);
        titreTolerance.setVisible(true);
        valeurTolerance.setVisible(true);
        uniteTolerance.setVisible(true);
        
    }
    
    public void setSixBande(){
        centaine.setItems(firstDigitList);
        dizaine.setItems(otherDigitList);
        unite.setItems(otherDigitList);
        multiplicateur.setItems(multiList);
        tolerance.setItems(toleranceList);
        temp.setItems(tempList);
        
        centaine.setVisible(true);
        dizaine.setVisible(true);
        unite.setVisible(true);
        multiplicateur.setVisible(true);
        tolerance.setVisible(true);
        temp.setVisible(true);

        bCentaine.setVisible(true);
        bDizaine.setVisible(true);
        bUnite.setVisible(true);
        bMult.setVisible(true);
        bTolerance.setVisible(true);
        bTemp.setVisible(true);

        lCentaine.setVisible(true);
        lDizaine.setVisible(true);
        lUnite.setVisible(true);
        lMulti.setVisible(true);
        lTolerance.setVisible(true);
        lTemp.setVisible(true); 
        
        titreResistance.setVisible(true);
        valeurResistance.setVisible(true);
        uniteResistance.setVisible(true);
        titreTolerance.setVisible(true);
        valeurTolerance.setVisible(true);
        uniteTolerance.setVisible(true);
        titreTemp.setVisible(true);
        uniteTemp.setVisible(true);
        valeurTemp.setVisible(true);
    }*/
    
    public void setNbBande(){
        if(nombreBande>=3){
            dizaine.setItems(firstDigitList);
            unite.setItems(otherDigitList);
            multiplicateur.setItems(multiList);

            dizaine.setVisible(true);
            unite.setVisible(true);
            multiplicateur.setVisible(true);
            bDizaine.setVisible(true);
            bUnite.setVisible(true);
            bMult.setVisible(true);


            lDizaine.setVisible(true);
            lUnite.setVisible(true);
            lMulti.setVisible(true);
            
            if(nombreBande<5){
                lDizaine.setText("1st Digit");
                lUnite.setText("2nd Digit");
            }

            titreResistance.setVisible(true);
            valeurResistance.setVisible(true);
            uniteResistance.setVisible(true);
            titreTolerance.setVisible(true);
            valeurTolerance.setVisible(true);
            uniteTolerance.setVisible(true);
            
            if(nombreBande==3){
                vTolerance = 20;
                valeurTolerance.setText(String.valueOf(vTolerance));
            }
        }
        if (nombreBande>=4){
            tolerance.setItems(toleranceList);

            tolerance.setVisible(true);
            bTolerance.setVisible(true);

            lTolerance.setVisible(true);

        }
        if (nombreBande>=5){
            centaine.setItems(firstDigitList);
            centaine.setVisible(true);
            bCentaine.setVisible(true);
            lCentaine.setVisible(true);
        }
        if (nombreBande>=6){
            temp.setItems(tempList);

            temp.setVisible(true);

            bTemp.setVisible(true);

            lTemp.setVisible(true); 

            titreTemp.setVisible(true);
            uniteTemp.setVisible(true);
            valeurTemp.setVisible(true);
        }
    }
    
    public void init(){
        centaine.setVisible(false);
        dizaine.setVisible(false);
        unite.setVisible(false);
        multiplicateur.setVisible(false);
        tolerance.setVisible(false);
        temp.setVisible(false);
        
        bCentaine.setVisible(false);
        bDizaine.setVisible(false);
        bUnite.setVisible(false);
        bMult.setVisible(false);
        bTolerance.setVisible(false);
        bTemp.setVisible(false);
        
        lCentaine.setVisible(false);
        lDizaine.setVisible(false);
        lUnite.setVisible(false);
        lMulti.setVisible(false);
        lTolerance.setVisible(false);
        lTemp.setVisible(false);
        
        valeurTolerance.setVisible(false);
        titreResistance.setVisible(false);
        valeurResistance.setVisible(false);
        uniteResistance.setVisible(false);
        titreTolerance.setVisible(false);
        titreTemp.setVisible(false);
        uniteTemp.setVisible(false);
        uniteTolerance.setVisible(false);
        valeurTemp.setVisible(false);
        
        valeurTemp.setText("");
        valeurTolerance.setText("");
        
        lCentaine.setText("1st Digit");
        lDizaine.setText("2nd Digit");
        lUnite.setText("3rd Digit");
        
        
        centaine.setValue("1 Marron");
        dizaine.setValue("1 Marron");
        unite.setValue("1 Marron");
        multiplicateur.setValue("1 Noir");
        tolerance.setValue("10 Argent");
        temp.setValue("200 Noir");
        
        cCentaine = centaine.getValue().split(" ");
        cDizaine = dizaine.getValue().split(" ");
        cUnite = unite.getValue().split(" ");
        cMult = multiplicateur.getValue().split(" ");
        cTolerance = tolerance.getValue().split(" ");
        cTemp = temp.getValue().split(" ");
        
        vTolerance = Double.valueOf(cTolerance[0]);
        vTemp = Double.valueOf(cTemp[0]);
        vMult =  Double.valueOf(cMult[0]);
        vCentaine = Double.valueOf(cCentaine[0]);
        vDizaine = Double.valueOf(cDizaine[0]);
        vUnite = Double.valueOf(cUnite[0]);
        
        bCentaine.setFill(setColor(cCentaine[1]));
        bDizaine.setFill(setColor(cDizaine[1]));
        bUnite.setFill(setColor(cUnite[1]));
        bMult.setFill(setColor(cMult[1]));
        bTolerance.setFill(setColor(cTolerance[1]));
        bTemp.setFill(setColor(cTemp[1]));
        
        valeurTolerance.setText(String.valueOf(vTolerance));
        valeurTemp.setText(String.valueOf(vTemp));
        
    }
    
    @FXML
    public void reload(){
        init();
        nbBande.setValue("");
    }
    
    public Color setColor(String couleur){
        switch (couleur) {
            case "Marron":
                return Color.BROWN;
            case "Noir":
                return Color.BLACK;
            case "Rouge":
                return Color.RED;
            case "Orange":
                return Color.ORANGE;
            case "Jaune":
                return Color.YELLOW;
            case "Vert":
                return Color.GREEN;
            case "Bleu":
                return Color.BLUE;
            case "Violet":
                return Color.PURPLE;
            case "Gris":
                return Color.GRAY;
            case "Argent":
                return Color.SILVER;
            case "Or":
                return Color.GOLD;
            case "Blanc":
                return Color.WHITE;
        }
        return null;
    }
    
    @FXML
    public void choiceBande(){
        nombreBande = Integer.parseInt(nbBande.getValue().substring(0,1));
        init();
        setNbBande();
        /*switch (nombreBande) {
            case 3:
                init();
                setTroisBande();
                break;
            case 4:
                init();
                
                setQuatreBande();
                break;
            case 5:
                init();
                
                setCinqBande();
                break;
            case 6:
                init();
                
                setSixBande();
                break;
            default:
                break;
        }*/
        vResitance=Calcul.choixCalcul();
        valeurResistance.setText(String.valueOf(vResitance));
    }

    @FXML
    public void choiceUnite(){
        cUnite = unite.getValue().split(" ");
        vUnite = Double.valueOf(cUnite[0]);
        vResitance=Calcul.choixCalcul();
        valeurResistance.setText(String.valueOf(vResitance));
        bUnite.setFill(setColor(cUnite[1]));
    }

    @FXML
    public void choiceDizaine(){
        cDizaine = dizaine.getValue().split(" ");
        vDizaine = Double.valueOf(cDizaine[0]);
        vResitance=Calcul.choixCalcul();
        valeurResistance.setText(String.valueOf(vResitance));
        bDizaine.setFill(setColor(cDizaine[1]));
    }

    @FXML
    public void choiceCentaine(){
        cCentaine = centaine.getValue().split(" ");
        vCentaine = Double.valueOf(cCentaine[0]);
        vResitance=Calcul.choixCalcul();
        valeurResistance.setText(String.valueOf(vResitance));
        bCentaine.setFill(setColor(cCentaine[1]));
    } 
  
    @FXML
    public void choiceMult(){
        cMult = multiplicateur.getValue().split(" ");
        vMult = Double.valueOf(cMult[0]);
        vResitance=Calcul.choixCalcul();
        valeurResistance.setText(String.valueOf(vResitance));
        bMult.setFill(setColor(cMult[1]));
    }

    @FXML
    public void choiceTolerance(){
        cTolerance = tolerance.getValue().split(" ");
        vTolerance = Double.valueOf(cTolerance[0]);
        valeurTolerance.setText(String.valueOf(vTolerance));
        bTolerance.setFill(setColor(cTolerance[1]));
    }

    @FXML
    public void choiceTemp(){
        cTemp = temp.getValue().split(" ");
        vTemp = Double.valueOf(cTemp[0]);
        valeurTemp.setText(String.valueOf(vTemp));
        bTemp.setFill(setColor(cTemp[1]));
    }

}