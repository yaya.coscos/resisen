/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.isen.resisen;

/**
 *
 * @author yann2
 */
public class Calcul {
    
     public static double choixCalcul(){
        switch (PrimaryController.nombreBande) {
            case 3:
                return calcul3();
            case 4:
                return calcul4();
            case 5:
                return calcul5();
            case 6:
                return calcul6();
            default:
                return 0;
        }
    }
    private static double calcul3(){
        PrimaryController.vTolerance = 20;
        return (10*PrimaryController.vDizaine+PrimaryController.vUnite)*PrimaryController.vMult;
    }

    private static double calcul4() {
        return (10*PrimaryController.vDizaine+PrimaryController.vUnite)*PrimaryController.vMult;
        //PrimaryController.valeurResistance.setText(String.valueOf(PrimaryController.vResitance));
    }

    private static double calcul5() {
        return (100*PrimaryController.vCentaine+10*PrimaryController.vDizaine+PrimaryController.vUnite)*PrimaryController.vMult;
    }

    private static double calcul6() {
        return (100*PrimaryController.vCentaine+10*PrimaryController.vDizaine+PrimaryController.vUnite)*PrimaryController.vMult;
    }
    
}
